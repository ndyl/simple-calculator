package dyl.natalia.calculator.actions;

import javax.swing.AbstractAction;
import javax.swing.JTextField;

public abstract class DisplayAction extends AbstractAction {
	
	private static final long serialVersionUID = -1028313605120007158L;
	protected JTextField placeToDisplay;
	
	public DisplayAction(String name, JTextField placeToDisplay) {
		super(name);
		this.placeToDisplay = placeToDisplay;
	}
	
	protected void setTextToDisplay(String textToDisplay) {
		placeToDisplay.setText(textToDisplay);
	}

}
