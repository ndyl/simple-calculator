package dyl.natalia.calculator.actions;

import java.awt.event.ActionEvent;
import java.util.regex.Matcher;

import javax.swing.JTextField;

import dyl.natalia.calculator.logic.Calculator;

public class PercentageAction extends CalculateAction {
	
	private static final long serialVersionUID = -3900658967831541485L;

	public PercentageAction(String name, JTextField placeToDisplay, Calculator calculator) {
		super(name, placeToDisplay, calculator);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String currentDisplayText = placeToDisplay.getText();
		Matcher intOrDoubleMatcher = createMatcherFor(INT_OR_DOUBLE_PATTERN, currentDisplayText);
		Matcher expressionMatcher = createMatcherFor(EXPRESSION_PATTERN, currentDisplayText);
		if(intOrDoubleMatcher.matches()) {
			double percent = getPercentValue(currentDisplayText);
			changeDisplayText(currentDisplayText, 0, percent);
		} else if (expressionMatcher.matches()) {
			int operatorIdx = getOperatorIdx(currentDisplayText);
			int startFrom = operatorIdx + 1;
			String numberStr = currentDisplayText.substring(startFrom);
			double percent = getPercentValue(numberStr);
			
			changeDisplayText(currentDisplayText, startFrom, percent);
		}
	}
	
	private double getPercentValue(String numberStr) {
		double number = Double.valueOf(numberStr);
		double percent = calculator.percentage(number);
		
		return percent;
	}

	private void changeDisplayText(String currentDisplayText, int startFrom, double percent) {
		StringBuilder editedText = new StringBuilder(currentDisplayText);
		editedText.replace(startFrom, currentDisplayText.length(), String.valueOf(percent));
		placeToDisplay.setText(editedText.toString());
	}
}
