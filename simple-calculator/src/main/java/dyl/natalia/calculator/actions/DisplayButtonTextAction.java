package dyl.natalia.calculator.actions;

import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JTextField;

public class DisplayButtonTextAction extends DisplayAction {

	private static final long serialVersionUID = -7431783919351548973L;
	
	public DisplayButtonTextAction(String name, JTextField placeToDisplay) {
		super(name, placeToDisplay);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton sourceBtn = (JButton) e.getSource();
		String currentDisplayText = placeToDisplay.getText();
		StringBuilder newDisplayText = new StringBuilder(currentDisplayText);
		newDisplayText.append(sourceBtn.getText());
		String textToDisplay = newDisplayText.toString();
		setTextToDisplay(textToDisplay);
	}
}