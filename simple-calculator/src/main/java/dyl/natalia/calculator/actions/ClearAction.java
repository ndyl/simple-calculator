package dyl.natalia.calculator.actions;

import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JTextField;

public class ClearAction extends DisplayAction {

	private static final long serialVersionUID = 6434792737146868960L;

	public ClearAction(String name, JTextField placeToDisplay) {
		super(name, placeToDisplay);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton source = (JButton) e.getSource();
		String btnText = source.getText();
		if(btnText.equals("CE")) {
			clearOneCharFromDisplay();
		} else {
			clearDisplay();
		}
	}
	
	private void clearDisplay() {
		setTextToDisplay("");
	}
	
	private void clearOneCharFromDisplay() {
		String currentText = placeToDisplay.getText();
		
		if(currentText.isEmpty()) {
			return;
		}
		
		String newText = currentText.substring(0, currentText.length()-1);
		setTextToDisplay(newText);
	}

}
