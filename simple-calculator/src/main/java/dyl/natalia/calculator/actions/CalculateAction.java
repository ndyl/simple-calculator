package dyl.natalia.calculator.actions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JTextField;

import dyl.natalia.calculator.logic.Calculator;

public abstract class CalculateAction extends DisplayAction {

	private static final long serialVersionUID = 4426457974967433450L;
	protected final String INT_OR_DOUBLE_PATTERN = "(-?)(0|([1-9]\\d*))(\\.\\d+)?";
	protected final String OPERATORS_PATTERN = "[\\+\\-\\*\\/]";
	protected final String EXPRESSION_PATTERN = "^" + INT_OR_DOUBLE_PATTERN + OPERATORS_PATTERN + INT_OR_DOUBLE_PATTERN + "$";
	protected Calculator calculator;
	
	public CalculateAction(String name, JTextField placeToDisplay, Calculator calculator) {
		super(name, placeToDisplay);
		this.calculator = calculator;
	}
	
	protected Matcher createMatcherFor(String pattern, String toMatch) {
		Pattern patternFromStr = Pattern.compile(pattern);
		Matcher matcher = patternFromStr.matcher(toMatch);
		
		return matcher;
	}
	
	protected int getOperatorIdx(String toMatch) {
		Matcher operatorMatcher = createMatcherFor(OPERATORS_PATTERN, toMatch);
		int operatorIdx = -1;
		if(operatorMatcher.find(1)) {
			operatorIdx = operatorMatcher.start();
		}
		
		return operatorIdx;
	}
	
	protected double calculateResult(double num1, double num2, char operator) {
		double result = 0.0;
		
		switch(operator) {
		case '+': 
			result = calculator.addition(num1, num2);
			break;
		case '-':
			result = calculator.subtraction(num1, num2);
			break;
		case '*': 
			result = calculator.multiplication(num1, num2);
			break;
		case '/': 
			result = calculator.division(num1, num2);
			break;
		}
		
		return result;
	}

}
