package dyl.natalia.calculator.actions;

import java.awt.event.ActionEvent;
import java.util.regex.Matcher;

import javax.swing.JButton;
import javax.swing.JTextField;

import dyl.natalia.calculator.logic.Calculator;

public class MemoryCalculationAction extends CalculateAction {

	private static final long serialVersionUID = 2145274851485321922L;

	public MemoryCalculationAction(String name, JTextField placeToDisplay, Calculator calculator) {
		super(name, placeToDisplay, calculator);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton source = (JButton) e.getSource();
		String operation = source.getText();
		calculate(operation);
	}
	
	private void calculate(String operation) {
		String currentDisplayText = placeToDisplay.getText();
		Matcher expressionMatcher = createMatcherFor(EXPRESSION_PATTERN, currentDisplayText);
		Matcher intOrDoubleMatcher = createMatcherFor(INT_OR_DOUBLE_PATTERN, currentDisplayText);
		double number = 0.0;
		
		if(expressionMatcher.matches()) {
			int operatorIdx = getOperatorIdx(currentDisplayText);
			String num1Str = currentDisplayText.substring(0, operatorIdx);
			String num2Str = currentDisplayText.substring(operatorIdx+1);
			
			char operator = currentDisplayText.charAt(operatorIdx);
			double num1 = Double.valueOf(num1Str);
			double num2 = Double.valueOf(num2Str);
			
			number = calculateResult(num1, num2, operator);
			setTextToDisplay(String.valueOf(number));
		} else if(intOrDoubleMatcher.matches()) {
			number = Double.valueOf(currentDisplayText);
		}
		
		switch(operation) {
		case "MC":
			calculator.clearStoredResult();
			break;
		case "MS": 
			calculator.storeResult(number);
			break;
		case "MR":
			double result = calculator.getStoredResult();
			setTextToDisplay(String.valueOf(result));
			break;
		case "M+":
			calculator.addToStoredResult(number);
			break;
		case "M-":
			calculator.subtractFromStoredResult(number);
			break;
		}
	}

}
