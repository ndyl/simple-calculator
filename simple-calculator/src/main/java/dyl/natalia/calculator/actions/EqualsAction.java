package dyl.natalia.calculator.actions;

import java.awt.event.ActionEvent;
import java.util.regex.Matcher;

import javax.swing.JTextField;

import dyl.natalia.calculator.logic.Calculator;

public class EqualsAction extends CalculateAction {

	private static final long serialVersionUID = -7431783919351548973L;
	
	public EqualsAction(String name, JTextField placeToDisplay, Calculator calculator) {
		super(name, placeToDisplay, calculator);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String currentDisplayText = placeToDisplay.getText();
		Matcher expressionMatcher = createMatcherFor(EXPRESSION_PATTERN, currentDisplayText);
		if(expressionMatcher.matches()) {
			int operatorIdx = getOperatorIdx(currentDisplayText);
			String num1Str = currentDisplayText.substring(0, operatorIdx);
			String num2Str = currentDisplayText.substring(operatorIdx+1);
			
			char operator = currentDisplayText.charAt(operatorIdx);
			double num1 = Double.valueOf(num1Str);
			double num2 = Double.valueOf(num2Str);
			
			double result = calculateResult(num1, num2, operator);		
			placeToDisplay.setText(String.valueOf(result));
		}
	}
}