package dyl.natalia.calculator.logic;

public class Calculator {
	private double storedResult;
	private double currentResult;
	
	public Calculator() {
		storedResult = 0.0;
		currentResult = 0.0;
	}
		
	public double addition(double firstAddend, double secondAddend) {
		currentResult = firstAddend + secondAddend;
		return currentResult;
	}
	
	public double addition(double ...addends) {
		double sum = 0.0;
		for(double addend : addends) {
			sum += addend;
		}
		return sum;
	}
	
	public double subtraction(double minuend, double subtrahend) {
		currentResult = minuend-subtrahend;
		return currentResult;
	}
	
	public double multiplication(double multiplicand, double multiplier) {
		currentResult = multiplicand*multiplier;
		return currentResult;
	}
	
	public double percentage(double percent) {
		return percent/100;
	}
	
	public double division(double dividend, double divisor) {
		if(checkIfZero(divisor)) {
			throw new IllegalArgumentException("Cannot divide by zero.");
		}
		currentResult = dividend/divisor;
		return currentResult ;
	}
	
	private boolean checkIfZero(double numToCheck) {
		return numToCheck == 0;
	}
	
	public void storeResult(double result) {
		this.storedResult = result;
	}
	
	public void clearStoredResult() {
		storedResult = 0.0;
	}
	
	public void clearCurrentResult() {
		currentResult = 0.0;
	}
	
	public void addToStoredResult(double addend) {
		storedResult += addend;
	}
	
	public void subtractFromStoredResult(double subtrahend) {
		storedResult -= subtrahend;
	}
	
	public double getStoredResult() {
		return storedResult;
	}
	
	public double getCurrentResult() {
		return currentResult;
	}
}
