package dyl.natalia.calculator.app;

import java.awt.*;

import javax.swing.*;

import dyl.natalia.calculator.helpers.ButtonsCreator;
import dyl.natalia.calculator.logic.Calculator;

public class CalculatorGUI extends JFrame {

	private static final long serialVersionUID = -4678810329567943276L;
	private final int BUTTONS_ROWS = 5;
	private final int INSET_SIZE = 3;
	private Calculator calculator;
	private JTextField placeToDisplay;

	public CalculatorGUI() {
		super("Simple Calculator");
		placeToDisplay = new JTextField();
		calculator = new Calculator();
		prepareFrame();
	}
	
	private JPanel addButtonsToPanel() {
		JPanel buttonsPanel = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(INSET_SIZE, INSET_SIZE, INSET_SIZE, INSET_SIZE);

		JButton[] memoryOperationsBtns = ButtonsCreator.createBtnsForMemoryOperations(placeToDisplay, calculator);
		JButton[] mathSymbolsBtns = ButtonsCreator.createMathSymbolsBtns(placeToDisplay, calculator);
		for(int i = 0; i < BUTTONS_ROWS; i++) {
			buttonsPanel.add(memoryOperationsBtns[i], gbc);
			gbc.gridx = 4;
			buttonsPanel.add(mathSymbolsBtns[i], gbc);
			gbc.gridx = 0;
			gbc.gridy += 1;
		}
		gbc.gridx = 3;
		gbc.gridy = 4;
		buttonsPanel.add(mathSymbolsBtns[5], gbc);

		gbc.gridx = 1;
		gbc.gridy = 0;
		JButton[] numbersAndDotBtns = ButtonsCreator.createNumbersAndDotButtons(placeToDisplay);
		for(int i = 1; i < numbersAndDotBtns.length; i++) {
			buttonsPanel.add(numbersAndDotBtns[i], gbc);

			if(isEndOfRow(i)) {
				gbc.gridx = 1;
				gbc.gridy += 1;
			} else {
				gbc.gridx += 1;
			}
		}

		gbc.gridwidth = 2;
		buttonsPanel.add(numbersAndDotBtns[0], gbc);

		gbc.gridx = 1;
		gbc.gridy = 4;
		gbc.gridwidth = 1;
		JButton[] clearBtns = ButtonsCreator.createClearBtns(placeToDisplay);
		for(int i = 0; i < clearBtns.length; i++) {
			buttonsPanel.add(clearBtns[i], gbc);
			gbc.gridx += 1;
		}

		return buttonsPanel;

	}

	private boolean isEndOfRow(int elementNumber) {
		return elementNumber % 3 == 0;
	}

	private void addElementsToFrame() {
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(INSET_SIZE+2, INSET_SIZE, INSET_SIZE+2, INSET_SIZE);
		JPanel buttons = addButtonsToPanel();
		buttons.setBackground(new Color(93, 64, 55));

		Dimension dimensionResultsField = new Dimension(buttons.getPreferredSize().width, 35);
		JTextField operationsAndResultsField = setPropertiesOfPlaceToDisplay(dimensionResultsField);
		this.add(operationsAndResultsField, gbc);

		gbc.gridy = 1;
		this.add(buttons, gbc);

	}

	private JTextField setPropertiesOfPlaceToDisplay(Dimension dimension) {
		placeToDisplay.setPreferredSize(dimension);
		placeToDisplay.setHorizontalAlignment(SwingConstants.RIGHT);
		placeToDisplay.setFont(new Font("Arial", Font.PLAIN, 16));
		placeToDisplay.setEditable(false);

		return placeToDisplay;
	}
	
	private void prepareFrame() {
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setSize(new Dimension(277, 295));
		setResizable(false);
		getContentPane().setBackground(new Color(93, 64, 55));
		setLayout(new GridBagLayout());
		addElementsToFrame();
		setLocationRelativeTo(null);
	}
}
