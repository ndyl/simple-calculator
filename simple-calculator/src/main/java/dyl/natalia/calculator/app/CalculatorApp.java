package dyl.natalia.calculator.app;

import javax.swing.SwingUtilities;

public class CalculatorApp {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				final CalculatorGUI calcGUI = new CalculatorGUI();
				calcGUI.setVisible(true);
			}
		});
	}
}
