package dyl.natalia.calculator.helpers;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JTextField;

import dyl.natalia.calculator.actions.*;
import dyl.natalia.calculator.logic.Calculator;

public class ButtonsCreator {
	
	private static final int BUTTON_HEIGHT = 25;
	private static final int BUTTON_WIDTH = 45;
	
	private ButtonsCreator() {
		
	}
	
	public static JButton[] createBtnsForMemoryOperations(JTextField placeToDisplay, Calculator calculator) {
		JButton[] memoryOperationBtns = new JButton[5];
		memoryOperationBtns[0] = new JButton(new MemoryCalculationAction("MC", placeToDisplay, calculator));
		memoryOperationBtns[1] = new JButton(new MemoryCalculationAction("MS", placeToDisplay, calculator));
		memoryOperationBtns[2] = new JButton(new MemoryCalculationAction("MR", placeToDisplay, calculator));
		memoryOperationBtns[3] = new JButton(new MemoryCalculationAction("M+", placeToDisplay, calculator));
		memoryOperationBtns[4] = new JButton(new MemoryCalculationAction("M-", placeToDisplay, calculator));

		Color backgroundColorBtn = new Color(141, 110, 99);
		Font fontBtn = new Font("Arial Narrow", Font.BOLD, 14);
		memoryOperationBtns = changeButtonsProperties(memoryOperationBtns, backgroundColorBtn, fontBtn);

		return memoryOperationBtns;
	}

	public static JButton[] createNumbersAndDotButtons(JTextField placeToDisplay) {
		JButton[] numbersAndDotBtns = new JButton[11];

		for(int i = 0; i < numbersAndDotBtns.length; i++) {
			JButton current = new JButton(new DisplayButtonTextAction(String.valueOf(i), placeToDisplay));
			numbersAndDotBtns[i] = current;
		}
		JButton dotBtn = new JButton(new DisplayButtonTextAction(".", placeToDisplay));
		numbersAndDotBtns[10] = dotBtn;

		Color backgroundColorBtn = new Color(215, 204, 200);
		Font fontBtn = new Font("Arial", Font.PLAIN, 16);
		numbersAndDotBtns = changeButtonsProperties(numbersAndDotBtns, backgroundColorBtn, fontBtn);
		numbersAndDotBtns[10].setFont(new Font("Arial", Font.BOLD, 16));

		return numbersAndDotBtns;
	}

	public static JButton[] createClearBtns(JTextField placeToDisplay) {
		JButton[] clearBtns = new JButton[2];
		clearBtns[0] = new JButton(new ClearAction("C", placeToDisplay));
		clearBtns[1] = new JButton(new ClearAction("CE", placeToDisplay));

		Color backgroundColorBtn = new Color(180, 70, 70);
		Font fontBtn = new Font("Arial Narrow", Font.ITALIC, 16);
		clearBtns = changeButtonsProperties(clearBtns, backgroundColorBtn, fontBtn);

		return clearBtns;
	}

	public static JButton[] createMathSymbolsBtns(JTextField placeToDisplay, Calculator calculator) {
		JButton[] mathSymbolsBtns = new JButton[6];
		mathSymbolsBtns[0] = new JButton(new DisplayButtonTextAction("+", placeToDisplay));
		mathSymbolsBtns[1] = new JButton(new DisplayButtonTextAction("-", placeToDisplay));
		mathSymbolsBtns[2] = new JButton(new DisplayButtonTextAction("*", placeToDisplay));
		mathSymbolsBtns[3] = new JButton(new DisplayButtonTextAction("/", placeToDisplay));
		mathSymbolsBtns[4] = new JButton(new PercentageAction("%", placeToDisplay, calculator));
		mathSymbolsBtns[5] = new JButton(new EqualsAction("=", placeToDisplay, calculator));

		Color backgroundColorBtn = new Color(188, 170, 164);
		Font fontBtn = new Font("Arial", Font.PLAIN, 16);
		mathSymbolsBtns = changeButtonsProperties(mathSymbolsBtns, backgroundColorBtn, fontBtn);
		JButton equalsBtn = mathSymbolsBtns[5];
		equalsBtn.setForeground(new Color(180, 70, 70));
		equalsBtn.setFont(new Font("Arial", Font.BOLD, 22));

		return mathSymbolsBtns;
	}

	private static JButton[] changeButtonsProperties(JButton[] buttonsToChange, Color backgroundColor, Font font) {
		JButton[] changedButtons = new JButton[buttonsToChange.length];
		
		for(int i = 0; i < changedButtons.length; i++) {
			JButton current = buttonsToChange[i];
			current.setPreferredSize(new Dimension(BUTTON_WIDTH, BUTTON_HEIGHT));
			current.setBackground(backgroundColor);
			current.setBorder(null);
			current.setFont(font);
			changedButtons[i] = current;
		}

		return changedButtons;
	}
}
