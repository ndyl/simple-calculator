package dyl.natalia.calculator.logic;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CalculatorTest {
	private Calculator calc;
	
	@Before
	public void setUp() {
		calc = new Calculator();
	}
	
	@Test
	public void addition_oneNegativeAndOnePositiveDouble_shouldReturnAdditionResult() {
		double results = calc.addition(-1.0, 2.2);
		assertEquals(1.2, results, 0.001);
	}
	
	@Test
	public void addition_multipleAddends_shouldReturnAdditionResult() {
		double results = calc.addition(-1.0, 2.0, -3, 12.0);
		assertEquals(10.0, results, 0.001);
	}

	@Test
	public void substraction_fromBiggerSubstractSmallerDouble_shouldReturnSubstractionResult() {
		double results = calc.subtraction(4.5, 1.5);
		assertEquals(3.0, results, 0.001);
	}
	
	@Test
	public void substraction_fromSmallerSubstractMuchBiggerDouble_shouldReturnSubstractionResult() {
		double results = calc.subtraction(1.5, 4.5);
		assertEquals(-3.0, results, 0.001);
	}
	
	@Test
	public void substraction_fromSmallNegativeSubstractBiggerNegative_shouldReturnPositiveResult() {
		double results = calc.subtraction(-1.0, -4);
		assertTrue(results > 0);
		assertEquals(3.0, results, 0.001);
	}
	
	@Test
	public void multiplication_twoPositiveDoubles_shouldReturnMultiplicationResult() {
		double results = calc.multiplication(3, 2);
		assertEquals(6.0, results, 0.001);
	}
	
	@Test
	public void multiplication_zeroMultAnyDouble_shouldReturnZero() {
		double results = calc.multiplication(0, 3.5);
		assertEquals(0.0, results, 0.001);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void division_divideByZero_shouldThrowIllegalArgumentException() {
		calc.division(3.0, 0);
	}
	
	@Test
	public void division_zeroDivideByAnyDouble_shouldReturnZero() {
		double results = calc.division(0, 3.5);
		assertEquals(0.0, results, 0.001);
	}
	
	@Test
	public void storeResult_doubleValueOf10_shouldStoredResultsEqualsDoubleValueOf10() {
		double result = 10.0;
		calc.storeResult(result);
		assertEquals(result, calc.getStoredResult(), 0.001);
	}
	
	@Test
	public void clearStoredResult_storedResultSetTo10_shouldStoredResultsEqualsZero() {
		double result = 10.0;
		calc.storeResult(result);
		calc.clearStoredResult();
		assertEquals(0.0, calc.getStoredResult(), 0.001);
	}
	
	@Test
	public void clearCurrentResult_currentResultSetTo10_shouldCurrentResultsEqualsZero() {
		double result = 10.0;
		calc.addition(0, result);
		calc.clearCurrentResult();
		assertEquals(0.0, calc.getCurrentResult(), 0.001);
	}
	
	@Test
	public void addToStoredResult_storedResultSetTo10ThenAdd3_shouldStoredResultsEquals13() {
		double result = 10.0;
		calc.storeResult(result);
		calc.addToStoredResult(3.0);
		assertEquals(13.0, calc.getStoredResult(), 0.001);
	}
	
	@Test
	public void subtractFromStoredResult_storedResultSetTo10ThenSub3_shouldStoredResultsEquals7() {
		double result = 10.0;
		calc.storeResult(result);
		calc.subtractFromStoredResult(3.0);
		assertEquals(7.0, calc.getStoredResult(), 0.001);
	}
	
	@Test
	public void percentage_50Percent_ShouldReturnFractionValue() {
		double result = calc.percentage(50);
		assertEquals(0.50, result, 0.001);
	}
	
	@Test
	public void fewOperationsTogether() {
		double expected = ((3+5)*2-2)/7;
		
		calc.addition(3, 5.0);
		calc.multiplication(calc.getCurrentResult(), 2);
		calc.subtraction(calc.getCurrentResult(), 2.0);
		calc.division(calc.getCurrentResult(), 7.0);
		
		assertEquals(expected, calc.getCurrentResult(), 0.001);
	}
}
